from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth import views as auth_views


from .views import(
    homeView,
    loginView,
    loginAuth,
    signinView,
    #registerView,
    user_create,
    fetch_all_users,
    fetch_messages,
    logout_new,






    #user_create,
    loggedin,
    invalid_login,
    #register,
    #user_detail,
    user_list,
    user_update,
    user_delete,

    profile,
    msg_resp,


#for message
    msg_create,

    message_detail,
    message_list,
    message_update,
    message_delete,
    fetch_countries,
    fetch_states,
    message_type,






)

urlpatterns = [





    #new work
    url(r'^homepage/$', homeView, name="home"),
    url(r'^login/$', loginView, name="login"),
    url(r'^auth_new/$', loginAuth, name = 'auth_new'),
    url(r'^auth_new/$', signinView, name="loggedin"),
    url(r'^logout/$', auth_views.logout, {'template_name': 'indexb.html'}, name='logout'),



    url(r'^signup/$', user_create, name = 'signup'),
    #url(r'^regi/$', user_create, name = 'register'),

    #url(r'^signups/$', signup, name="signsup"),












    #url(r'^auth/$', signin, name = 'auth'),
    url(r'^autht/$', msg_create, name='create_message'),
    url(r'^authr/$', msg_resp, name='create_resp'),
    url(r'^invalid/', invalid_login, name='invalid_log'),
    url(r'^dashboard/$', loggedin, name='userpage'),

    #url(r'^profile/(?P<id>\d+)/$', user_detail, name = 'profile'),
    url(r'^profile/(?P<id>\d+)/$', profile, name='profile'),
    url(r'^list_usr/$', user_list, name = 'list_user'),
    url(r'^edit_profile/(?P<id>\d+)/$', user_update, name = 'edit_user'),
    url(r'^(?P<id>\d+)/delete_usr/$', user_delete, name = 'delete_user'),






    #message


    url(r'^msg/$', message_list, name = 'list_message'),
    url(r'^(?P<parent>\d+)/msg/$', message_detail, name = 'detail_message'),
    url(r'^(?P<id>\d+)/edit_message/$', message_update, name='update_message'),
    url(r'^(?P<id>\d+)/delete_message/$', message_delete, name='delete_message'),
    url(r'^fetch_countries/$', fetch_countries),
    url(r'^fetch_states/$', fetch_states),



    url(r'^all_user/$', fetch_all_users, name='all_user'),
    url(r'^all_msg/$', fetch_messages, name='all_user'),
    url(r'^msg_type/$', message_type, name='msg_type'),


]

