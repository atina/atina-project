from __future__ import unicode_literals
from django.core.validators import RegexValidator
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import models
from django import template

register = template.Library()

# Create your models here.
def upload_location(instance, filename):
    return "%s/%s" %(instance.id, filename)

class Country(models.Model):
    name = models.CharField(max_length=50)
    class Meta:
        db_table = 'tbl_country'

    def __unicode__(self):
        return self.name

    def __str__(self):
        return "%s" % (self.name)

    def as_json(self):
        return dict(
            id = self.id,
            name=self.name
        )

class State(models.Model):
    country=models.IntegerField()
    name = models.CharField(max_length=35)
    class Meta:
        db_table = 'tbl_state'

    def __unicode__(self):
        return self.name

    def __str__(self):
        return "%s" % (self.name)

    def as_json(self):
        return dict(
            id = self.id,
            country=self.country,
            name=self.name


        )


class UserProfile(models.Model):
    user = models.OneToOneField(User, null=True)
    TITLE_CHOICES = (
        ('Mr', 'Mr'),
        ('Mrs', 'Mrs'),
        ('Miss', 'Miss'),

    )
    title = models.CharField(max_length=5, choices=TITLE_CHOICES, null=True)
    address = models.CharField(max_length=200, null=True)
    address_2 = models.CharField(max_length=200, null=True)
    address_3 = models.CharField(max_length=200, null=True)
    mobile = models.CharField(max_length=15, null=True)
    image = models.ImageField(upload_to='photos/%Y/%m/%d')
    country = models.ForeignKey(Country, on_delete=models.CASCADE, null=True)
    state = models.ForeignKey(State, on_delete=models.CASCADE, null=True)
    zipcode = models.CharField(max_length=50, null=True, blank=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        db_table = 'userprofile'


    def __unicode__(self):
        return self.user.username

    def __str__(self):
        return "%s" % (self.user.username)

    def get_absolute_url(self):
        return reverse("atinapp:detail_user", kwargs={"id":self.id})

    def as_json(self):
        return dict(
            user=str(self.user),#[self.user.first_name,self.user.last_name,self.user.username,self.user.email],
            title=self.title,
            address=self.address,
            address_2=self.address_2,
            address_3 = self.address_3,
            mobile= self.mobile,
            image = str(self.image),
            country = str(self.country),
            state = str(self.state),
            zipcode = self.zipcode,





        )

class MessageType(models.Model):
    msg_type = models.CharField(max_length=255)
    timestamp = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        db_table = 'messagetype'

    def __unicode__(self):
        return self.msg_type

    def __str__(self):
        return self.msg_type

    def as_json(self):
        return dict(
            id=self.id,
            msg_type = self.msg_type,
            timestamp = str(self.timestamp)
        )

class Message(models.Model):
        user = models.IntegerField()
        msg_type = models.IntegerField()
        msg_content = models.TextField()
        parent = models.IntegerField(null=True, default=0)
        updated = models.DateTimeField(auto_now=True, auto_now_add=False)
        timestamp = models.DateTimeField(auto_now=True, auto_now_add=False)

        class Meta:
            db_table = 'message'

        def __unicode__(self):
            return self.msg_content

        def __str__(self):
            return self.msg_content

        def as_json(self):
            return dict(
                id = self.id,
                user=self.user,
                msg_type=self.msg_type,
                msg_content=self.msg_content,
                parent = self.parent

            )


class Staff(models.Model):
    name = models.CharField(max_length=20)


    class Meta:
        db_table = 'staff'

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    def as_json(self):
        return dict(
            id=self.id,
            name = self.name

        )
