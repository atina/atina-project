from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .forms import UserProfileForm, UserForm,MessageForm,StaffForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from django.contrib.auth.models import User
from .models import UserProfile,Country,State,Staff
from .models import Message, MessageType
from django.contrib.auth import authenticate, login
from django.shortcuts import render_to_response
from django.contrib.auth import authenticate, login
from django.views.decorators.csrf import csrf_exempt
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.contrib import auth



import hashlib
import json



# for new Return home page
def homeView(request):
    return render(request, "indexb.html", {})

#sign in view
def signinView(request):
    return render(request, "indexc.html", {})

#login view
def loginView(request):
    return render(request, "login.html", {})

#login authentication
@csrf_exempt
def loginAuth(request):
    if request.method == "GET":
        return render(request, 'login.html')
    elif request.method == "POST":
        username =  request.POST.get('username');
        password =  request.POST.get('password');
        user = authenticate(username =  username, password = password)
        if user is not None:
            request.session.set_expiry(86400)
            user.session = request.session
            login(request, user)
            return render(request,'indexc.html')
        else:
            return HttpResponse('wrong username or password')

#signup view
# def registerView(request):
#     return render(request, "signup.html", {})

#sign up authentication for forms.py
@csrf_exempt
def user_create(request):
    if request.method=="GET":
        return render(request, "signup.html")
    elif request.method=="POST":
        title = request.POST.get('title')
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        email = request.POST.get('email')
        username = request.POST.get('username')
        password = request.POST.get('password')
        mobile = request.POST.get('mobile')
        address = request.POST.get('address')
        address_2 = request.POST.get('address_2')
        address_3 = request.POST.get('address_2')
        country_id = request.POST.get('country')
        state_id =  request.POST.get('state')
        zipcode = request.POST.get('zipcode')
        image = request.FILES.get('image')
        user = User(first_name=first_name, last_name=last_name, password=password, username=username, email=email)
        user.set_password(User.objects.make_random_password())
        userprofile = UserProfile(address=address,address_2=address_2,address_3=address_3, country_id=country_id, state_id=state_id, zipcode=zipcode, mobile=str(mobile), image=str(image))
        #return HttpResponse(title + " " + first_name + " " + last_name + " " + password + " " + username + " " + email + " " + address + " " + address_2 + " " + str(country_id) + " " + str(state_id) + " " + zipcode + " " + str(mobile) + " " + str(image) + " ")
        user.save()
        userprofile.user = user
        userprofile.save()
        return render(request, 'indexb.html')


#fetch countries and return json
def fetch_countries(request):
    if request.method == "GET":
        queryset = Country.objects.all()
        resp_json = [ob.as_json() for ob in queryset]
        return HttpResponse(json.dumps(resp_json))

#fetch states and return json
def fetch_states(request):
    if request.method == "GET":
        queryset = State.objects.all()
        resp_json = [ob.as_json() for ob in queryset]
        return HttpResponse(json.dumps(resp_json))

#fetch message and return json
def fetch_messages(request):
    if request.method == "GET":
        queryset = Message.objects.all()
        resp_json = [ob.as_json() for ob in queryset]
        return HttpResponse(json.dumps(resp_json))


#fetch users and return json
def fetch_all_users(request):
    if request.method == "GET":
        queryset = UserProfile.objects.all()
        resp_json = [ob.as_json() for ob in queryset]
        return HttpResponse(json.dumps(resp_json))



def loggedin (request):
    return render_to_response('indexb.html',
							  {'username': request.user.username})
def invalid_login(request):
    return render_to_response('invalid_login.html')


def logout_new(request):
    auth.logout(request)
    return redirect("home")






# qs_type = MessageType.objects.all()
#     qs_user = User.objects.all()
#     qs_msg = Message.objects.all().order_by("-timestamp")
#
#     paginator = Paginator(qs_msg, 3)
#
#     page = request.GET.get('page')
#     try:
#         queryset = paginator.page(page)
#     except PageNotAnInteger:
#         queryset = paginator.page(1)
#     except EmptyPage:
#         queryset = paginator.page(paginator.num_pages)
#
#     context = {
#         "page_list":queryset,
#         "message_types": qs_type,
#         "user_lists": qs_user
#     }


def profile(request, id):
    if id == "0":
        if request.user.is_authenticated:
            user = User.objects.get(id=id)
    else:
        user = User.objects.get(id=id)
        context = {
            "user":user,
        }


    return render_to_response('profile.html', context,  RequestContext(request))


def user_list(request):
    queryset = User.objects.all()
    context = {
        "object_lists":queryset,
    }
    return render(request, "user_list.html", context)

@csrf_exempt
def user_update(request, id=None):
    instance = get_object_or_404(User, id=id)
    form = UserForm(request.POST or None)
    form1 = UserProfileForm(request.POST or None, request.FILES or None)
    if form.is_valid() * form1.is_valid():
        user = form.save()
        userprofile = form1.save(commit=False)
        userprofile.user = user
        userprofile.save()
        # message success
        messages.success(request, "Account created Successfully, you can now login")

    context = {
         "form":form,
        "form1":form1,

    }

    return render(request, "edit_profile.html", context)

def user_delete(request, id=None):
    instance = get_object_or_404(User, id=id)
    instance.delete()
    return render(request, "index.html", {})


#message views


# def create_message(request):
#     form = MessageForm(request.POST or None)
#     if form.is_valid():
#         message = form.save()
#     context = {
#         "form":form,
#     }
#     return render(request, "message.html", context)


@csrf_exempt
def msg_create(request):
    if request.method=="POST":
        qs_type = MessageType.objects.all()
        qs_user = User.objects.all()
        qs_msg = Message.objects.all().order_by("-timestamp")
        paginator = Paginator(qs_msg, 3)
        page = request.GET.get('page')
        try:
            queryset = paginator.page(page)
        except PageNotAnInteger:
            queryset = paginator.page(1)
        except EmptyPage:
            queryset = paginator.page(paginator.num_pages)
        context = {
            "page_lists": queryset,
            "messages_types": qs_type,
            "users_lists": qs_user,
        }
        #user_id = User.objects.get(id=id)
        userid = request.POST.get('userid')
        msg_type = request.POST.get('msg_type')
        parent = request.POST.get('parent', 0)
        msg_content = request.POST.get('msg_content')
        #return HttpResponse(userid + " " + msg_type + " " + str(parent) + " " + msg_content + " " )
        msg = Message(user = userid, msg_type = msg_type, parent = parent, msg_content = msg_content)
        msg.save()
        return render(request, 'dashboard.html', context)

#for response
@csrf_exempt
def msg_resp(request):
    if request.method == "POST":
        qs_type = MessageType.objects.all()
        qs_user = User.objects.all()
        qs_msg = Message.objects.all().order_by("-timestamp")
        paginator = Paginator(qs_msg, 3)
        page = request.GET.get('page')
        try:
            queryset = paginator.page(page)
        except PageNotAnInteger:
            queryset = paginator.page(1)
        except EmptyPage:
            queryset = paginator.page(paginator.num_pages)
        context = {
            "page_lists": queryset,
            "messages_types": qs_type,
            "users_lists": qs_user,
        }
        userid = request.POST.get('userid')
        msg_type = request.POST.get('msg_type')
        parent = request.POST.get('parent')
        msg_content = request.POST.get('msg_content')
        #return HttpResponse(userid + " " + msg_type + " " + str(parent) + " " + msg_content + " " )
        msgs = Message(user=userid, msg_type=msg_type, parent=parent, msg_content=msg_content)
        msgs.save()
        return render(request, 'dashboard.html', context)


def message_detail(request, parent=None):
    instance = Message.objects.filter(parent=parent)

    context = {
        "instance": instance
    }
    return render(request, "dashboard.html", context)

def message_list(request):
    if request.method=="GET":
        queryset = Message.objects.all()
        resp_json = [ob.as_json() for ob in queryset]
        return HttpResponse(json.dumps(resp_json))

def message_type(request):
    if request.method=="GET":
        queryset = MessageType.objects.all()
        resp_json = [ob.as_json() for ob in queryset]
        return HttpResponse(json.dumps(resp_json))


def message_update(request, id=None):
    instance = get_object_or_404(Message, id=id)
    form = MessageForm(request.POST or None, instance=instance)
    if form.is_valid():
        message = form.save()
    context = {
        "form": form,
        "instance":instance,
    }
    return render(request, "comment.html", context)

def message_delete(request, id=None):
    instance = get_object_or_404(Message, id=id)
    instance.delete()
    return render(request, "message.html", {})


def message_detail(request, parent=None):
    instance = Message.objects.filter(parent=parent)
    resp_json = [ob.as_json() for ob in instance]
    return HttpResponse(json.dumps(resp_json))





