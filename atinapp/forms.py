from django import forms
from .models import User, UserProfile,Message, Country,State,Staff


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    class Meta:
        model = User
        fields = [
            "first_name",
            "last_name",
            "email",
            "password",
            "username",

        ]

    def save(self, commit=True):
        user = super(UserForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile

        fields = [
            "title",
            "address",
            "address_2",
            "address_3",
            "country",
            "state",
            "zipcode",
            "mobile",
            "image",

        ]





        #message

class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = [
            "user",
            "msg_type",
            "msg_content",
            #"compstatus",
            "parent",
        ]


def save(self, commit=True):
    message = super(MessageForm, self).save(commit=False)

    if commit:
        message.save()
    return message


class StaffForm(forms.ModelForm):
    class Meta:
        model = Staff
        fields=[
            "name",

        ]