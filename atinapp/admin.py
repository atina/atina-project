from django.contrib import admin
from .models import UserProfile, MessageType,Message,Staff

# Register your models here.
class UserProfileAdmin(admin.ModelAdmin):
    list_display = ["user","updated", "timestamp"]
    list_display_links= ["updated"]
    search_fields =["user"]
    class Meta:
        model = UserProfile
admin.site.register(UserProfile, UserProfileAdmin)

class MessageTypeAdmin(admin.ModelAdmin):
    class Meta:
        model = MessageType
admin.site.register(MessageType, MessageTypeAdmin)

class MessageAdmin(admin.ModelAdmin):
    class Meta:
        model = Message
admin.site.register(Message, MessageAdmin)

class StaffAdmin(admin.ModelAdmin):
    class Meta:
        model = Staff
admin.site.register(Staff, StaffAdmin)