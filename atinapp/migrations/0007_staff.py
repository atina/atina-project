# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-03-09 13:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('atinapp', '0006_auto_20170309_1121'),
    ]

    operations = [
        migrations.CreateModel(
            name='Staff',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.IntegerField()),
                ('age', models.CharField(max_length=20)),
            ],
            options={
                'db_table': 'staff',
            },
        ),
    ]
